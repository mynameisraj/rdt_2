all:
	gcc receiver_main.c -o reliable_receiver -g -std=gnu99
	gcc sender_main.c -o reliable_sender -g -std=gnu99
clean:
	rm -rf *.dSYM *.o reliable_sender reliable_receiver output.txt
