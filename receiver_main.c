#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>

#define MTU 1472
#define ACK_SIZE sizeof(unsigned long long int)
#define NUM_BYTES MTU-ACK_SIZE
    // Num bytes per payload packet
#define RECV_TIMEOUT_S 1

// Debugging
#define SIM_SLOWNESS 0
#define DELAY 20000
#define DROP_TEST 0
#define DROP_COUNT 500

typedef struct pkt_struct {
    unsigned long long int seqnum;
    char payload[NUM_BYTES];
} pkt_t;

// Get the file descriptor set up for our socket. From Beej's guide
static void start_listener(int* sockfd, unsigned short int myUDPport) {
    struct addrinfo hints, *servinfo, *p;
    int rv;

    char* portno = malloc(sizeof(char)*6);
    sprintf(portno, "%d", myUDPport);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, portno, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    }

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((*sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("listener: socket");
            continue;
        }

        if (bind(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(*sockfd);
            perror("listener: bind");
            continue;
        }

        break;
    }

    struct timeval tv;
    tv.tv_sec = RECV_TIMEOUT_S;
    tv.tv_usec = 0;
    setsockopt(*sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

    if (p == NULL) {
        fprintf(stderr, "listener: failed to bind socket\n");
    }

    freeaddrinfo(servinfo);
    free(portno);
}

// Receieve data on the given port, writing it to the file
static void reliablyReceive(unsigned short int myUDPport, char* destinationFile) {
    int sockfd;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    socklen_t addr_len;
    addr_len = sizeof their_addr;
    start_listener(&sockfd, myUDPport);
    fprintf(stderr, "Listening on %d\n", myUDPport);

    FILE* file = fopen(destinationFile, "wb");

    unsigned long long int next = 0;
        // Expected next sequence
    int got_first_ack = 0;
    unsigned long long num_dupacks = 0;
    pkt_t packet;
    unsigned long long count = 0;

    while (1) {
        if ((numbytes = recvfrom(sockfd, &packet, sizeof(pkt_t) , 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                goto cleanup;
            } else {
                fprintf(stderr, "Error in recvfrom:%s\n", strerror(errno));
            }
        }
        printf("RECV %llu - ", packet.seqnum);
        count++;
        if (DROP_TEST && count % DROP_COUNT == 0) {
            printf("DROP %llu (DEBUG)\n", packet.seqnum);
            continue;
        }

        if (packet.seqnum == next) {
            // Received a packet in order
            got_first_ack = 1;
            fwrite(packet.payload, numbytes-ACK_SIZE, 1, file);
            printf("ACK %llu (GOOD)\n", next);
            if (SIM_SLOWNESS) {
                usleep(DELAY);
            }
            sendto(sockfd, &next, ACK_SIZE, 0, (struct sockaddr *)&their_addr, addr_len);
            next++;
        } else if (got_first_ack && packet.seqnum > next) {
            // Got an out-of-order packet. Send an ACK for the last receieved packet
            num_dupacks++;
            unsigned long long int recent = next - 1;
            printf("ACK %llu (BAD)\n", recent);
            if (SIM_SLOWNESS) {
                usleep(DELAY);
            }
            sendto(sockfd, &recent, ACK_SIZE, 0, (struct sockaddr *)&their_addr, addr_len);
        } else {
            // Receieved a packet with lower sequence number than we care about
            printf("IGNORE\n");
        }
    }

cleanup:
    fprintf(stderr, "------------------------\n\
Dupacks: %llu\n------------------------\n", num_dupacks);
    if (next == 0) next = 1;
        // Avoid printing ULL_MAX
    fprintf(stderr, "Wrote %lu bytes (%llu packets)\n", ftell(file), next);
    fclose(file);
    close(sockfd);
}

int main(int argc, char** argv) {
    unsigned short int udpPort;

    if(argc != 3)
    {
        fprintf(stderr, "usage: %s UDP_port filename_to_write\n\n", argv[0]);
        exit(1);
    }

    udpPort = (unsigned short int)atoi(argv[1]);

    reliablyReceive(udpPort, argv[2]);
}
