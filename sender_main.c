#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>

#define MTU 1472
    // Max transmit unit
#define PKT_HEADER sizeof(unsigned long long int)
#define NUM_BYTES MTU-PKT_HEADER
    // Num bytes per payload packet
#define INITIAL_WINDOW_SIZE 10
#define RECV_TIMEOUT 50000
#define MAX_FAILS 100
    // How many successive timeouts before quit

typedef struct pkt_struct {
    unsigned long long int seqnum;
    char payload[NUM_BYTES];
} pkt_t;

// Set up the socket. From Beej's guide
static struct addrinfo* init_connection(char* hostname, unsigned short int hostUDPport,
                            int* sockfd) {
    struct addrinfo hints, *servinfo, *p;
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    char* port = malloc(sizeof(char)*6);
    sprintf(port, "%d", hostUDPport);

    if ((rv = getaddrinfo(hostname, port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return NULL;
    }

    // Loop through all the results and make a socket
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((*sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            fprintf(stderr, "talker: socket");
            continue;
        }

        break;
    }

    // Set timeout
    struct timeval tv;
    tv.tv_usec = RECV_TIMEOUT;
    tv.tv_sec = 0;
    setsockopt(*sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

    if (p == NULL) {
        fprintf(stderr, "talker: failed to bind socket\n");
        return NULL;
    }
    free(port);
    return p;
}

// Transfer to port on hostname the number of bytes from filename
void reliablyTransfer(char* hostname, unsigned short int hostUDPport, char* filename, unsigned long long int bytesToTransfer) {
    // Get connection up and running
    int sockfd = -1;
    struct addrinfo* p = init_connection(hostname, hostUDPport, &sockfd);
    fprintf(stderr, "Sending on %d\n", hostUDPport);

    // Initialization
    FILE* file = fopen(filename, "rb");
    unsigned long long int window_size = INITIAL_WINDOW_SIZE;
    unsigned long long int base = 0;
        // "front" of window
    unsigned long long int next = 0;
        // Next sequence number
    unsigned long long int transferred = 0;
        // Total sent count, bytes
    int slow_start = 1;
        // Are we in slow start?
    unsigned long long int ssthresh = 45;
        // Threshold to end slow start
    int dupack_count = 0;
        // Number of successive dupacks. Resets every 3
    int data_size = NUM_BYTES;
        // Bytes to send per packet
    int suc_fails = 0;
        // Number of successive timeouts we have had
    unsigned long long int num_acks = 0;
        // Number of ACK since last window increase
    unsigned long long int last_packet = 0;
        // Last packet sent
    unsigned long long int final_packet = 0;
    unsigned long long int last_ack = 0;
    int reached_eof = 0;

    // Some statistics
    unsigned long num_halves = 0;
        // Number of times the congestion window has halved
    unsigned long long num_dupacks = 0;
        // total number of dupacks

    // Start the send loop
    if (bytesToTransfer < data_size) {
        data_size = (int)bytesToTransfer;
    }

    // Readjust the window size if it needs to be smaller
    if (bytesToTransfer < window_size*data_size) {
        window_size = (bytesToTransfer + data_size - 1) / data_size;
    }

    unsigned long long int default_window_size = window_size;
        // The lowest value the window size can be
    unsigned long last_packet_size = data_size;


    while (!reached_eof && next < base + window_size) {
        pkt_t packet;
        size_t bytes_read = fread(packet.payload, 1, data_size, file);
        // fread returns 0 if we reached end of file
        if (bytes_read == 0) {
            long curr = ftell(file);
            fseek(file, 0, SEEK_END);
            long end = ftell(file);
            fseek(file, curr, SEEK_SET);
            bytes_read = (size_t)(end-curr);
            printf("Hit EOF at packet %llu | %llu\n", next, window_size);
            reached_eof = 1;
            last_packet = next;
            final_packet = last_packet;
            last_packet_size = bytes_read;
        }
        packet.seqnum = next;
        sendto(sockfd, &packet, bytes_read+PKT_HEADER, 0, p->ai_addr, p->ai_addrlen);
        printf("  SEND SEQ %llu | %llu\n", packet.seqnum, window_size);
        next++;
    }

    // Main loop
    while (1) {
        if (transferred >= bytesToTransfer) {
            goto cleanup;
        }

        // Send next packet in window
        while (!reached_eof && next < base + window_size) {
            pkt_t packet;
            size_t bytes_read = fread(packet.payload, 1, data_size, file);
            // fread returns 0 if we reached end of file
            if (bytes_read == 0) {
                long curr = ftell(file);
                fseek(file, 0, SEEK_END);
                long end = ftell(file);
                fseek(file, curr, SEEK_SET);
                bytes_read = (size_t)(end-curr);
                printf("Hit EOF at packet %llu | %llu\n", next, window_size);
                reached_eof = 1;
                last_packet = next;
                final_packet = last_packet;
                last_packet_size = bytes_read;
            }
            packet.seqnum = next;
            sendto(sockfd, &packet, bytes_read+PKT_HEADER, 0, p->ai_addr, p->ai_addrlen);
            printf("SEND SEQ %llu | %llu\n", packet.seqnum, window_size);
            next++;
        }

        // Check if there's anything to receieve
        unsigned long long int seq;
            // Received sequence number
        ssize_t num_recv;
            // In bytes
        if ((num_recv = recvfrom(sockfd, &seq, PKT_HEADER,
                                 0, p->ai_addr, &p->ai_addrlen)) != -1) {
            suc_fails = 0;
            if (seq >= base) {
                // In-order ACK. Advance the window
                dupack_count = 0;
                last_ack = seq;
                base = seq+1;
                if (reached_eof && seq == final_packet) {
                    transferred = seq*data_size+last_packet_size;
                    goto cleanup;
                }
                transferred = (seq+1)*data_size;
                if (slow_start) {
                    window_size++;
                    if (window_size >= ssthresh) {
                        slow_start = 0;
                        printf("END SLOW START\n");
                    }
                } else if (num_acks >= window_size-1) {
                    window_size++;
                    num_acks = 1;
                } else {
                    num_acks++;
                }
                printf("  ");
            } else {
                // Duplicate ACK. Send from base to next
                dupack_count++;
                num_dupacks ++;
                if (dupack_count == 3) {
                    if (slow_start) {
                        ssthresh = window_size/2;
                        window_size = default_window_size;
                        num_halves++;
                    } else {
                        // Reduce window by 2
                        window_size = window_size/2 + 3;
                        num_halves++;
                    }
                    next = base;
                    dupack_count = 0;
                }
                fseek(file, base*data_size, SEEK_SET);
                printf("RE");
            }
        } else if (num_recv != 0 && seq > last_ack) {
            // Handle timeout. Resend n packets from base to window size
            suc_fails++;
            if (suc_fails >= MAX_FAILS) {
                goto cleanup;
            }

            if (slow_start) {
                ssthresh = window_size/2;
                window_size = default_window_size;
            } else {
                // Reduce window by 2
                window_size = window_size/2 + 3;
                num_halves++;
            }
            dupack_count = 0;            
            next = base;

            fseek(file, base*data_size, SEEK_SET);
            printf("TO");
        } else {
            printf("  ");
        }
    }
cleanup:
    fprintf(stderr, "------------------------\nDecreases: \
%lu\nDupacks: %llu\n------------------------\n", num_halves, num_dupacks);
    if (transferred >= bytesToTransfer) {
        fprintf(stderr, "Transferred %llu bytes\n", bytesToTransfer);
    } else {
        fprintf(stderr, "WARNING - only transferred %llu bytes\n", transferred);
    }
    fclose(file);
    freeaddrinfo(p);
    close(sockfd);
}

int main(int argc, char** argv) {
    unsigned short int udpPort;
    unsigned long long int numBytes;

    if(argc != 5)
    {
        fprintf(stderr, "usage: %s receiver_hostname receiver_port filename_to_xfer bytes_to_xfer\n\n", argv[0]);
        exit(1);
    }
    udpPort = (unsigned short int)atoi(argv[2]);
    numBytes = atoll(argv[4]);

    reliablyTransfer(argv[1], udpPort, argv[3], numBytes);
} 
